https://acho25.gitlab.io/tic-tac-toe/
# Tictactoe

Implémentation C++/JavaScript du jeu de Tictactoe.


- Nom: Achour
- Prénom: Youness
- Groupe: G1


## Compilation du projet C++

Projet CMake classique, dans le dossier `cpp` :

```
nix-shell
mkdir build
cmake ..
make
...
```

## Compilation du projet JavaScript

Dans le dossier `js` :


```
nix-shell
make
make run
...
```


