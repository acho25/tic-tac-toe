with import <nixpkgs> {};

emscriptenStdenv.mkDerivation {

  name = "tictactoe-wasm";

  src = ./.;

  buildInputs = [
    gnumake
    python3
    python3Packages.httpserver
    emscriptenPackages.zlib
  ];

  buildPhase = ''
    make html
  '';

  installPhase = ''
    mkdir -p $out/public
    cp -r build/html/* $out/public/
  '';




}



