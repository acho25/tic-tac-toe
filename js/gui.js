"use strict";



function joueurVersTexte(joueur) {
    switch (joueur) {
        case Module.Joueur.VIDE: return "vide";
        case Module.Joueur.ROUGE: return "rouge";
        case Module.Joueur.VERT: return "vert";
        case Module.Joueur.EGALITE: return "égalité";
    }
}


function joueurVersCss(joueur) {
    switch (joueur) {
        case Module.Joueur.ROUGE: return "a joueurRouge";
        case Module.Joueur.VERT: return "a joueurVert"
        default: return "a joueurVide";
    }
}


const Gui = function(jeu) {


        

    this.jeu = jeu;
    console.log(jeu.show());
    var table_xo = document.getElementById('table-xo');

    let row = 0;

    let col = 0;
    var i = 0;
    var x =0;
    var c = '';
    var messsage = '';
    
    
   
 

    for (row = 0; row < 3; row++) {
        for (col = 0; col < 3; col++) {
            

            var div = document.createElement('div');
            div.className = 'table_td';
            c = "c" + i;
            div.setAttribute('id', c);
            jeu.raz();
            let nrow = row;
            let ncol = col;
            let nc = c;
            //console.log("nrow = " + nrow + ", ncol = " + ncol + ", nc = " + nc);
            
            div.addEventListener("click", () => this.played(nrow, ncol, nc));
    
            
            //jeu.jouer(row,col);

            table_xo.appendChild(div);
            i++;
        }
    }
    
    // ...


    // crée le bouton
    const dom_bouton = document.getElementById('recommencer');
    //dom_bouton.innerHTML = "recommencer";
    dom_bouton.onclick = _ => this.gererBouton();
    //document.body.appendChild(dom_bouton);


    // reinitialise le jeu et l'interface
    
//this.razGui();
//this.played();
   
    

};


Gui.prototype.gererBouton = function() {
    this.messsage='joueur courant :  ' + joueurVersTexte(Module.Joueur.VERT);
    this.razGui();
};

Gui.prototype.razGui = function(){
    var row = 0;
    var col = 0;
    var i = 0;
    let c = 'c';
    this.jeu.raz();
    for (row = 0; row < 3; row++) {
        for (col = 0; col < 3; col++) {
            c = 'c' + i;
            document.getElementById(c).style.backgroundColor = 'white';
            i ++;
        }
    }
 
};

Gui.onTileClick = function(i){
    console.log(`Tile clicked : ${i}`)
};




Gui.prototype.played = function (row, col, id) {
    
 
    //console.log('CLICKED row = ' + row + ', col = ' + col);
    
    var w =0;
    if (this.jeu.jouer(row,col)) {
        
        if  (document.getElementById(id)) document.getElementById(id).style.backgroundColor = round;  
          
        if(round == 'red') {
            this.messsage ='joueur courant :  ' + joueurVersTexte(Module.Joueur.VERT);
            this.jeu.playrouge(row, col);
            if(this.jeu.getVainqueur()==Module.Joueur.ROUGE){
                w=2;
                this.messsage = 'vainqueur: '+joueurVersTexte(Module.Joueur.ROUGE);
                //document.write(this.messsage);
                console.log('rouge a gagner!!');
            }

            round = 'green';
        } else if(round == 'green') {
            this.messsage = 'joueur courant :  ' + joueurVersTexte(Module.Joueur.ROUGE);
            this.jeu.playvert(row, col);
            if(this.jeu.getVainqueur()==Module.Joueur.VERT){
                w=2;
                this.messsage = 'vainqueur: '+joueurVersTexte(Module.Joueur.VERT);
               // document.write(this.messsage);
                console.log('vert a gagner!!');}

            round = 'red';
        }
        if(this.jeu.getVainqueur()==Module.Joueur.EGALITE) {
            w=2;
            this.messsage = 'vainqueur: '+joueurVersTexte(Module.Joueur.EGALITE) ;
            }

    }
 


    else
    {
        this.messsage = "tu peux pas jouer ici!!!" ;
        console.log('you cant play here !!!');}
    
    console.log(this.jeu.show());
    document.getElementById("message").innerHTML=this.messsage;

};
