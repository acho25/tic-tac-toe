/// \mainpage 
///  implementation c++ du jeu de tictactoe
///
/// utilisation typique: 
///
/// \code{.cpp}
/// Jeu jeu;
/// Joueur vainqueur = jeu.getVainqueur();
/// while(vainqueur = VIDE) {
///     Joueur joueurCourant = jeu.getJoueurCourant();
///     jeu.jouer(i, j);
///     vainqueur = jeu.getVainqueur();       
/// }
/// \endcode
