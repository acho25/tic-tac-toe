#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
}

void Jeu::raz() {
    // TODO
    // TODO
    for (int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            tab[i][j]='.';
        }

    }
}

void Jeu::show(){

    for (int i=0;i<3;i++){
        for(int j=0;j<3;j++){
        std::cout<<tab[i][j];

        }
        std::cout<<std::endl ;

    }

}

bool Jeu::full() const{

    for (int i=0;i<3;i++){
        for(int j=0;j<3;j++){
        if(tab[i][j]=='.')return false ;
        }
    

    }
    return true ;
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
    // TODO
    return os;
}

Joueur Jeu::getVainqueur() const {
    int i =0;
    // si le vert gagne 
    if(tab[0][0]=='V' and tab[0][1]=='V' and tab[0][2]=='V')  return Joueur(2);
    if(tab[1][0]=='V' and tab[1][1]=='V' and tab[1][2]=='V') return Joueur(2) ;
    if(tab[2][0]=='V' and tab[2][1]=='V' and tab[2][2]=='V') return Joueur(2);
    if(tab[0][0]=='V' and tab[1][0]=='V' and tab[2][0]=='V') return Joueur(2);
    if(tab[0][1]=='V' and tab[1][1]=='V' and tab[2][1]=='V') return Joueur(2);
    if(tab[0][2]=='V' and tab[1][2]=='V' and tab[2][2]=='V') return Joueur(2);
    if(tab[0][0]=='V' and tab[1][1]=='V' and tab[2][2]=='V') return Joueur(2);
    if(tab[0][2]=='V' and tab[1][1]=='V' and tab[2][0]=='V') return Joueur(2);
    if(tab[0][1]=='V' and tab[1][1]=='V' and tab[2][1]=='V') return Joueur(2);

    //si le rouge gagne 
    if(tab[0][0]=='R' and tab[0][1]=='R' and tab[0][2]=='R')  return Joueur(1);
    if(tab[1][0]=='R' and tab[1][1]=='R' and tab[1][2]=='R') return Joueur(1);
    if(tab[2][0]=='R' and tab[2][1]=='R' and tab[2][2]=='R') return Joueur(1);
    if(tab[0][0]=='R' and tab[1][0]=='R' and tab[2][0]=='R') return Joueur(1);
    if(tab[0][1]=='R' and tab[1][1]=='R' and tab[2][1]=='R') return Joueur(1);
    if(tab[0][2]=='R' and tab[1][2]=='R' and tab[2][2]=='R') return Joueur(1);
    if(tab[0][0]=='R' and tab[1][1]=='R' and tab[2][2]=='R') return Joueur(1);
    if(tab[0][2]=='R' and tab[1][1]=='R' and tab[2][0]=='R') return Joueur(1);
    if(tab[0][1]=='R' and tab[1][1]=='R' and tab[2][1]=='R') return Joueur(1);

    //si egalité
    if(  full()== false ) return Joueur(0); // jeu pas finis
    return Joueur(3) ;




}

Joueur Jeu::getJoueurCourant() const {
    // TODO
    if(joueur_courrant==Joueur(1)) return Joueur(1);
    if(joueur_courrant==Joueur(2)) return Joueur(2);
    else return JOUEUR_VIDE;
}

bool Jeu::jouer(int i, int j) {
    if(i>2 or i<0 or tab[i][j]!='.' ) return false ;
    else return true ;
}

