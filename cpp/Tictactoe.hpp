#ifndef TICTACTOE_HPP
#define TICTACTOE_HPP

#include <array>
#include <iostream>

enum Joueur { JOUEUR_VIDE, JOUEUR_ROUGE, JOUEUR_VERT, JOUEUR_EGALITE };

class Jeu {
    private:
        // TODO

    public:
    char tab[3][3];
    Joueur joueur_courrant;
        // Constructeur à utiliser.
        Jeu();

        // Retourne le vainqueur (ROUGE, VERT, EGALITE) ou VIDE si partie en cours.
        Joueur getVainqueur() const;

        // Retourne le joueur (ROUGE ou VERT) qui doit jouer.
        Joueur getJoueurCourant() const;

        // Joue un coup pour le joueur courant.
        // 
        // i ligne choisie (0, 1 ou 2)
        // j colonne choisie (0, 1 ou 2)
        // 
        // Si le coup est invalide, retourne false. Si le coup est valide,
        // joue le coup et retourne true.
        bool jouer(int i, int j);

        // Réinitialise le jeu.
        void raz();
        // afficher la grilee
        void show();
        bool full() const;
        int test(int x){return x;};
        void playvert(int x , int y){
            if(jouer(x,y)==true) tab[x][y]= 'V';
        };
        void playrouge(int x , int y){
            if(jouer(x,y)==true)tab[x][y]= 'R';
        };

        friend std::ostream & operator<<(std::ostream & os, const Jeu & jeu);
};

std::ostream & operator<<(std::ostream & os, const Jeu & jeu);

#endif

/**
 * 
 * 
 * <div class="table_td" data-index="0" id="c0" onclick="played(0, 0, 'c0')"></div>
        <div class="table_td" data-index="1" id="c1" onclick="played(0, 1, 'c1')"></div>
        <div class="table_td" data-index="2" id="c2" onclick="played(0, 2, 'c2')"></div>
        <div class="table_td" data-index="3" id="c3" onclick="played(1, 0, 'c3')"></div>
        <div class="table_td" data-index="4" id="c4" onclick="played(1, 1, 'c4')"></div>
        <div class="table_td" data-index="5" id="c5" onclick="played(1, 2, 'c5')"></div>
        <div class="table_td" data-index="6" id="c6" onclick="played(2, 0, 'c6')"></div>
        <div class="table_td" data-index="7" id="c7" onclick="played(2, 1, 'c7')"></div>
        <div class="table_td" data-index="8" id="c8" onclick="played(2, 2, 'c8')"></div>
 * 
*/

