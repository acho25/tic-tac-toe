{ pkgs ? import <nixpkgs> {} }:

with pkgs; clangStdenv.mkDerivation {
  name = "tictactoe";
  src = ./.;
  buildInputs = [
    cmake
    catch2
    doxygen
    pkgconfig
  ];

    buildPhase = ''
    make html
  '';

  installPhase = ''
    mkdir -p $out/public
    cp -r build/html/* $out/public/
  '';
}



