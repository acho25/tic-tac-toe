#include "Tictactoe.hpp"
#include <sstream>

#include <catch2/catch.hpp>

TEST_CASE("tableau") { 
    Jeu jeu ;
    std::string s ;
    std::string z ="...\n...\n...\n";

    for (int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            if((i==0 and j==2) or (i==1 and j==2) or (i==2 and j==2)) s=s+jeu.tab[i][j]+"\n";
            else s=s+jeu.tab[i][j];
        }
    }
        REQUIRE(s == z);
    }

 
    TEST_CASE("coup invalide") { 
        Jeu jeu ;
            REQUIRE(jeu.jouer(8,4) == false);
        }
    
    TEST_CASE("jouer emplacement pas bon ") { 
        Jeu jeu ;
        jeu.tab[1][1]='x';
            REQUIRE(jeu.jouer(1,1) == false );
    }


    TEST_CASE("jouer emplacement  bon ") { 
        Jeu jeu ;
        jeu.tab[1][1]='x';
            REQUIRE(jeu.jouer(1,2) == true );
    }


    TEST_CASE("test vinqueur_VERT") { 
        Jeu jeu ;
        jeu.tab[0][0]='V';
        jeu.tab[0][1]='V';
        jeu.tab[0][2]='V';
        jeu.tab[1][0]='V';
        jeu.tab[1][1]='R';
        jeu.tab[1][2]='R';
        jeu.tab[2][0]='R';
        jeu.tab[2][1]='V';
        jeu.tab[2][2]='V';
        
        
         REQUIRE(jeu.getVainqueur() ==  Joueur(2));
      
        }

    TEST_CASE("test vinqueur_ROUGE") { 
        Jeu jeu ;
        jeu.tab[0][0]='R';
        jeu.tab[0][1]='R';
        jeu.tab[0][2]='R';
        jeu.tab[1][0]='R';
        jeu.tab[1][1]='V';
        jeu.tab[1][2]='V';
        jeu.tab[2][0]='V';
        jeu.tab[2][1]='R';
        jeu.tab[2][2]='R';
        
        
         REQUIRE(jeu.getVainqueur() ==  Joueur(1));
      
        }



    TEST_CASE("test vinqueur_egalité") { 
        Jeu jeu ;
        jeu.tab[0][0]='R';
        jeu.tab[0][1]='V';
        jeu.tab[0][2]='R';
        jeu.tab[1][0]='R';
        jeu.tab[1][1]='V';
        jeu.tab[1][2]='V';
        jeu.tab[2][0]='V';
        jeu.tab[2][1]='R';
        jeu.tab[2][2]='V';
        
         REQUIRE(jeu.getVainqueur() ==  Joueur(3));
      
        }
  


